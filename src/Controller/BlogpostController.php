<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Repository\BlogpostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogpostController extends AbstractController
{
    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualites(
        BlogpostRepository $blogpost,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $blogpost->findBy([], ['id' => 'DESC']);

        $actus = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            3
        );
        return $this->render('blogpost/actualites.html.twig', [
            'actualites' => $actus,
        ]);
    }

    /**
     * @Route("actualites/{slug}", name="actualite_detail")
     */
    public function detailBlogpost(Blogpost $blogpost): Response
    {
        return $this->render('blogpost/details.html.twig', [
            'blogpost' => $blogpost,
        ]);
    }
}
