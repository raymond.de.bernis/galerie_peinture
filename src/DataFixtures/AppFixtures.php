<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $user = new User();

        $user->setEmail('user@test.com')
            ->setPrenom($faker->firstName())
            ->setNom($faker->lastName())
            ->setTelephone($faker->phoneNumber())
            ->setApropos($faker->text())
            ->setInstagram('instagram')
            ->setRoles(['ROLE_PEINTRE']);

        $password = $this->encoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // création de 10 blogposts
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();
            $blogpost->setTitre($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setContenu($faker->text(350))
                ->setSlug($faker->slug(3))
                ->setUser($user);

            $manager->persist($blogpost);
        }

        // un blogpost pour les Tests
        $blogpost = new Blogpost();
        $blogpost->setTitre('Blogpost test')
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setContenu($faker->text(350))
            ->setSlug('blogpost-test')
            ->setUser($user);

        $manager->persist($blogpost);

        // création des catégories et des peintures
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();
            $categorie->setNom($faker->word())
                ->setDescription($faker->words(10, true))
                ->setSlug($faker->slug());

            $manager->persist($categorie);

            //création 2 peintures par catégorie
            for ($j = 0; $j < 2; $j++) {
                $peinture = new Peinture();
                $peinture
                    ->setNom($faker->words(3, true))
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->setHauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                    ->setcreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('portfolio-3.jpg')
                    ->addCategory($categorie)
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setUser($user);

                $manager->persist($peinture);
            }
        }
        //categorie de test
        $categorie = new Categorie();
        $categorie->setNom('categorie test')
            ->setDescription($faker->words(10, true))
            ->setSlug('categorie-test');

        $manager->persist($categorie);

        //Peinture de test
        $peinture = new Peinture();
        $peinture
            ->setNom('peinture test')
            ->setLargeur($faker->randomFloat(2, 20, 60))
            ->setHauteur($faker->randomFloat(2, 20, 60))


            ->setEnVente($faker->randomElement([true, false]))
            ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
            ->setcreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text())
            ->setPortfolio($faker->randomElement([true, false]))
            ->setSlug('peinture-test')
            ->setFile('portfolio-3.jpg')
            ->addCategory($categorie)
            ->setPrix($faker->randomFloat(2, 100, 9999))
            ->setUser($user);

        $manager->persist($peinture);

        $manager->flush();
    }
}
