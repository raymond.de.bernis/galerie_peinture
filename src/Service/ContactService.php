<?php

namespace App\Service;

use DateTime;
use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ContactService
{
    private $manager;
    private $flash;
    private $session;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function persistContact(Contact $contact): void
    {
        $session = new Session();

        $contact->setIsSend(false)
            ->setCreatedAt(new DateTime('now'));
        $this->manager->persist($contact);
        $this->manager->flush();

        $this->session->start();
        $this->session->getFlashBag()->add('success', 'Votre message est bien envoyé, merci.');
    }

    public function isSend(Contact $contact): void
    {
        $contact->setIsSend(true);

        $this->manager->persist($contact);
        $this->manager->flush();
    }
}
