<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTime;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class CommentaireTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $createdAt = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();


        $commentaire
            ->setAuteur('auteur')
            ->setEmail('email@test.com')
            ->setContenu('contenu')
            ->setCreatedAt($createdAt)
            ->setBlog($blogpost)
            ->setPeinture($peinture);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getCreatedAt() === $createdAt);
        $this->assertTrue($commentaire->getBlog() === $blogpost);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
    }
    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $createdAt = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();


        $commentaire
            ->setAuteur('auteur')
            ->setEmail('email@test.com')
            ->setContenu('contenu')
            ->setCreatedAt($createdAt)
            ->setBlog(new Blogpost())
            ->setPeinture(new Peinture());

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@test.com');
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getBlog() === $blogpost);
        $this->assertFalse($commentaire->getPeinture() === $peinture);
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getBlog());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getId());
    }
}
