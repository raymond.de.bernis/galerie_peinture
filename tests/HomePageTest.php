<?php

namespace App\Tests;

//use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\Panther\PantherTestCase;

class HomePageTest extends PantherTestCase
//class HomePageTest extends WebTestCase
{
    public function testShoulDisplayHomePage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        //$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'RAYMOND DE BERNIS');
    }
}
